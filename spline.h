#ifndef SPLINE_H
#define SPLINE_H

#include <list>
#include "point.h"

class Spline {
public:
	std::list<Point> points;
	std::list<Point> pPoints;

	void calculate(const Point& newPoint) {
		auto pointsIter = points.begin();
		auto pPointsIter = pPoints.begin();
		Point newPPoint;

		while (pointsIter != points.end()) {
			auto nextPointIter = pointsIter;
			++nextPointIter;
			if (nextPointIter == points.end() || 
				newPoint.x < pointsIter->x || 
				needRecalculateP(*pointsIter, *nextPointIter, newPoint)) {
				newPPoint = calculateP();
			}

			if (nextPointIter == points.end()) {
				points.push_back(newPoint);
				pPoints.push_back(newPPoint);
			} else if (newPoint.x < pointsIter->x) {
				points.push_front(newPoint);
				pPoints.push_back(newPPoint);
			} else {
				points.insert(pointsIter, newPoint);
				pPoints.insert(pPointsIter, newPPoint);
			}

			++pointsIter;
			if (pPointsIter != pPoints.end()) {
				++pPointsIter;
			}
		}
	}

private:
	Point calculateP() {

	}

	bool needRecalculateP(const Point& currentPoint,
		const Point& nextPoint, const Point& newPoint) {
		return newPoint.x > currentPoint.x && newPoint.x < nextPoint.x;
	}
};
#endif // ! SPLINE_H