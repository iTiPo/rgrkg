#ifndef POINT_H
#define POINT_H

class Point {
public:
	float x;
	float y;
	
	Point(const float& x = 0.0f, const float& y = 0.0f) {
		this->x = x;
		this->y = y;
	}

	void setX(const float& x) {
		this->x = x;
	}

	void setY(const float& y) {
		this->y = y;
	}

	void set(const float& x, const float& y) {
		setX(x);
		setY(y);
	}

	void set(const Point& otherPoint) {
		set(otherPoint.x, otherPoint.y);
	}
};

#endif // !POINT_H